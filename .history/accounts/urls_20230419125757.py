from django.urls import path
from accounts.views import user_login, logout_view

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_, name="logout"),
]
