from django.contrib import admin
from receipts.models import 

@admin.register(ExpenseCategory)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe",
        "step_number",
        "id"
    )
