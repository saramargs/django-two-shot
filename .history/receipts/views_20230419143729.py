from django.shortcuts import render
from receipts.models import Receipt


def show_receipts(request):
    receipt = Receipt.objects.all()
    context = {"receipt": receipt}
    return render(request, "receipts/list.html", context)

def 