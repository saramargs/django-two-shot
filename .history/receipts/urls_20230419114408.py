from django.urls import path
from receipts.views import show_receipts

urlpatterns = [
    path('', show_receipts, name="show_receipts"),
    # path('<int:id>/', show_recipe, name="show_recipe"),
    # path('create/', create_recipe, name="create_recipe"),
]