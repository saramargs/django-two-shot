from django.urls import path
from receipts.views import show_receipts, create_view

urlpatterns = [
    path("", show_receipts, name="home"),
    path("", create_view, name="create_receipt"),
]
