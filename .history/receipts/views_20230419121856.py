from django.shortcuts import render
from receipts.models import Receipt
from django.shortcuts import redirect


def show_receipts(request):
    receipt = Receipt.objects.all()
    context = {"receipt": receipt}
    return render(request, "receipts/list.html", context)
