from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required


def show_receipts(request):
    receipt = Receipt.objects.all()
    context = {"receipt": receipt}
    return render(request, "receipts/list.html", context)


@login_required(redirect_field_name='login')
def create_view(request):
    if request.method == "POST":
        form = 