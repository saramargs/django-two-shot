from django.urls import path
from receipts.views import show_receipts

urlpatterns = [
    path('', show_receipts, name="list_recipes"),
    # path('<int:id>/', show_recipe, name="show_recipe"),
    # path('create/', create_recipe, name="create_recipe"),
    # path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
]