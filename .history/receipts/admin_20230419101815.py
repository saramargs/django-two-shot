from django.contrib import admin
from receipts.

@admin.register(ExpenseCategory)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe",
        "step_number",
        "id"
    )
