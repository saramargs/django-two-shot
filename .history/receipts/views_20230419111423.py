from django.shortcuts import render
from models import Receipt


def show_receipt(request):
  receipt = Receipt.objects.all()
  context = {
    "receipt": receipt
  }
  return render(request, "receipts/list.html", context)