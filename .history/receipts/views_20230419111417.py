from django.shortcuts import render
from models import Receipt


def show_receipt(request):
  receipt = Receipt.objects.all()
  context = {
    "receipt": receipt
  }
  return render(request, "model_names/list.html", context)