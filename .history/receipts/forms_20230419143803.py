from django import forms


class ReceiptForm(forms.Form):
    vend = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
