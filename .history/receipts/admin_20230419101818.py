from django.contrib import admin
from receipts.models

@admin.register(ExpenseCategory)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe",
        "step_number",
        "id"
    )
