from django.urls import path
from receipts.views import show_receipts, create_receipt, category_list

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("category/", category_list, name="category_list"),
    path("accounts/", category_list, name="category_list"),
]
