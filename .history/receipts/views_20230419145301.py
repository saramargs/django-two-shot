from django.shortcuts import render, redirect
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm


def show_receipts(request):
    receipt = Receipt.objects.all()
    context = {"receipt": receipt}
    return render(request, "receipts/list.html", context)


@login_required(redirect_field_name="login")
def create_view(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm(instance=    )
