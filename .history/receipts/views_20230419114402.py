from django.shortcuts import render
from receipts.models import Receipt


def home(request):
    return redirect("show_receipts")

def show_receipts(request):
    receipt = Receipt.objects.all()
    context = {"receipt": receipt}
    return render(request, "receipts/list.html", context)
