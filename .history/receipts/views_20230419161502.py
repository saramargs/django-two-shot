from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm


@login_required(redirect_field_name="login")
def show_receipts(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": receipt}
    return render(request, "receipts/list.html", context)


@login_required(redirect_field_name="login")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {"form": form}
        return render(request, "receipts/create.html", context)


@login_required(redirect_field_name="login")
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category": category}
    return render(request, "receipts/categories.html", context)


@login_required(redirect_field_name="login")
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"account": account}
    return render(request, "receipts/categories.html", context)
