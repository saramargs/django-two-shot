from django import forms
from receipts.models import Receipt

class ReceiptForm(forms.Form):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category"
        )
